package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
)

type Identity struct {
	Greeting         string
	InstanceId       string
	InstanceType     string
	PrivateIp        string
	AvailabilityZone string
    PublicIp         string

}

func main() {

	http.HandleFunc("/", serveResp())
	http.ListenAndServe(":8889", nil)
}

func serveResp() func(http.ResponseWriter, *http.Request) {

	var identity Identity

	if err := getInstanceDets(&identity); err == nil {
		if index, err := template.ParseFiles("template.html"); err == nil {
			identity.Greeting = "Greetings from EC2"
			fmt.Println(identity.InstanceId)
			return func(w http.ResponseWriter, r *http.Request) {
				if err := index.Execute(w, identity); err != nil {
					log.Println(err)
					fmt.Fprintln(w, "Fatal error")
				}
			}
		} else {
			log.Println(err)
		}
	}

	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Fatal error")
	}
}

func getInstanceDets(identity *Identity) (err error) {
	if res, err := http.Get("http://169.254.169.254/latest/dynamic/instance-identity/document"); err == nil {
		if res.StatusCode == 200 {
			if bytStr, err := ioutil.ReadAll(res.Body); err == nil {
				json.Unmarshal(bytStr, &identity)
				fmt.Println("%s", identity)
				res.Body.Close()
			} else {
				log.Println(err)
				return err
			}
		} else {
			log.Println(res)
			return err
		}
	} else {
		log.Println(err)
		return err
	}
	
    if res, err := http.Get("http://169.254.169.254/latest/meta-data/public-hostname"); err == nil {
            if res.StatusCode == 200 {
                    if bytStr, err := ioutil.ReadAll(res.Body); err == nil {
                            fmt.Println(string(bytStr))
                            identity.PublicIp = string(bytStr)
                            res.Body.Close()
                    } else {
                            log.Println(err)
                            return err
                    }
            } else {
                    log.Println(res)
                    return err
            }
    } else {
            log.Println(err)
            return err
    }

	return
}

/*
func getInstanceDets(identity *Identity) error {
	res, err := ioutil.ReadFile("jsonData.json")
	json.Unmarshal(res, &identity)
	fmt.Println(identity)
	return err
}
*/
